mixin LoginValidation {
  String? validateEmail(String? email){
    if(email!.isEmpty){
      return 'Email is required.';
    }
    final regex = RegExp('[^@]+@[^\.]+\..+');
    if(!regex.hasMatch(email)){
      return 'Enter a valid email.';
    }
    return null;
  }

  String? validatePassword(String? password){
    if(password!.length < 6){
      return 'Password has at least 6 characters.';
    }
  }
}