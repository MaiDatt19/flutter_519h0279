import 'package:flutter/material.dart';
import 'package:week6/ui/login.dart';
import 'package:week6/ui/stopwatch.dart';


class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/':(context) => LoginScreen(),
        '/login':(context)=>LoginScreen(),
        StopwatchScreen.route: (context) => StopwatchScreen()
      },
      initialRoute: '/',
    );
  }

}