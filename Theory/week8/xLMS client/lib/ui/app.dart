import 'package:flutter/material.dart';
import 'package:xlmsclient/ui/login.dart';

class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "xLMS client",
      routes: {
        "/":(context) => AppHomePage(),
        "/login":(context) => LoginScreen(),
      },
      initialRoute: "/",
    );
  }

}

class AppHomePage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return AppHomePageState();
  }

}

class AppHomePageState extends State<AppHomePage>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Text('Chưa biết ghi gì hết'),
    );
  }
}