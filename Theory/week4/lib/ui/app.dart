import 'package:flutter/material.dart';
import 'package:week4/bloc/bloc.dart';
import 'package:week4/validation/mixin_common_validation.dart';


class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Login Me",
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Login'),
        ),
        body: LoginScreen(),
      ),
    );
  }

}

class LoginScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }

}

class LoginState extends State<StatefulWidget> with CommonValidation{
  final formKey = GlobalKey<FormState>();
  late String email;
  late String password;

  final bloc = Bloc();
  String errorMessage = '';


  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            emailField(),
            passwordField(),
            loginButton(),
          ],
        )
      )
    );
  }

  Widget emailField(){
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        icon: Icon(Icons.mail),
        labelText: "Email address",
        errorText: errorMessage),
      //validator: validateEmail,
      onSaved: (value){
        email = value as String;
      },
      onChanged: (String value){
        print('onChanged email: $value');
        // bloc.emailStreamController.sink.add(value);
        // bloc.getEmailStreamController().sink.add(value);
        //bloc.changeEmail(value);
        //bloc.emailStream.lis

        if(!value.contains('@')){
          setState(() {
            errorMessage = '$value is an invalid email';
          });
        }
        else{
          errorMessage = '';
        }
        setState(() {
          print('Update the screen state');
        });
      },
    );
  }

  Widget passwordField(){
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
        icon: Icon(Icons.lock),
        labelText: "Password",
        ),
        
      validator: validatePassword,
      onSaved: (value){
        print('onSaved: value=$value');
      },
    );
  }

  Widget loginButton(){
    return ElevatedButton(
      onPressed: (){
        if(formKey.currentState!.validate()){
          formKey.currentState!.save();
        }
      }, 
      child: Text('Login'));
  }

}