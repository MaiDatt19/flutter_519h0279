import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../model/image_model.dart';
import 'dart:convert';

class App extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return AppState();
  }


}

class AppState extends State<App>{
  int counter = 0;

  fetchImage(){
    print('Hi there: counter = $counter');

    Uri url = Uri.https('jsonplaceholder.typicode.com', 'photos/$counter');
    http.get(url).then((result){
      var jsonR = result.body;
      var jsonObject = json.decode(jsonR);
      var imageModel = ImageModel.fromJson(jsonObject);
      print(imageModel);
    });

    setState(() {
      counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    var appWidget = new MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text('Image Viewer - v0.0.7'),),
        body: Text('Display list of images here, counter = $counter'),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: fetchImage),
        ),
    );
    return appWidget;
  }
}
