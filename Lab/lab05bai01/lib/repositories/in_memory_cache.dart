import 'repositories.dart';

class InmemoryCache implements Repository{
  final storage = Map<int, Model>();

  @override
  void clear() {
    storage.clear();
  }

  @override
  Model create() {
    final ids = storage.keys.toList()..sort();
    final id = (ids.length==0)?1: ids.last + 1;

    final model = Model(id: id);
    storage[id] = model;
    return model;
  }

  @override
  Model? delete(int id) {
    return storage.remove(id);
  }

  @override
  Model? get(int id) {
    return storage[id];
  }

  @override
  List<Model> getAll() {
    return storage.values.toList(growable: false);
  }

  @override
  int update(Model item) {
    storage[item.id as int] = item;
    return 0;
  }



}