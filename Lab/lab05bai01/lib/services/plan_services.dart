import '../repositories/repositories.dart';
import '../models/task.dart';
import '../models/plan.dart';
import '../repositories/in_memory_cache.dart';

class PlanServices {
  Repository repo = InmemoryCache();

  Plan createPlan(String name){
    final model = repo.create();
    final plan = Plan.fromModel(model)..name = name;
    savePlan(plan);
    return plan;
  }

  void  savePlan(Plan plan){
    repo.update(plan.toModel());
  }

  void delete(Plan plan){
    repo.delete(plan.toModel().id);
  }

  List<Plan> getAllPlans(){
    return repo.getAll().map<Plan>((model) => Plan.fromModel(model)).toList();
  }

  void addTask(Plan plan, String description){
    final id = (plan.tasks.length > 0) ? plan.tasks.last.id : 1;
    final task = Task(id:id, description: description);
    plan.tasks.add(task);
    savePlan(plan);
  }

  void deleteTask(Plan plan, Task task){
    plan.tasks.remove(task);
    savePlan(plan);
  }

}