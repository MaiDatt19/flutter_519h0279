import 'package:flutter/foundation.dart';
import '../repositories/repositories.dart';
import 'task.dart';


class Plan{
  int id=0;
  String name='';
  List<Task> tasks = [];

  Plan({
    required this.id, this.name =''
  });

  Plan.fromModel(Model model){
    id = model.id;
    name = model.data['name'] ?? '';
    if(model.data['task'] != null){
      tasks = model.data['task'].map<Task>((task) => Task.fromModel(task)).toList();
    }
  }

  Model toModel() => Model(id:id, data: {
    'name': name,
    'task':tasks.map((task) =>task.toModel()).toList()
  });

  int get completerCount => tasks.where((task) => task.complete).length;

  String get completenessMessage => '$completerCount out of ${tasks.length} tasks';

}