import 'dart:math';

import 'package:flutter_test/flutter_test.dart';
import '../../lib/models/task.dart';
import '../../lib/repositories/repositories.dart';

void main(){
  test('New task from model', (){
    var model = Model(id: 1, data:{
      'description':'Create a flutter app same as Tiki',
      'complete':false
    });

    var task = Task.fromModel(model);

    expect(task.id, 1);
    expect(task.description, 'Create a flutter app same as Tiki');
    expect(task.complete, false);


  });
}