import 'package:flutter/material.dart';
import 'package:bignumberutil/bignumberutil.dart';

class BignumberUtilScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return BignumberUtilScreenState();
  }

}

class BignumberUtilScreenState extends State<BignumberUtilScreen>{
  var num1Controller = TextEditingController();
  var num2Controller = TextEditingController();
  var result = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Text("ENTER 2 NUMBERS", style: TextStyle(fontSize: 30, color: Colors.black)),
          SizedBox(height: 30,),
          TextField(
            decoration: InputDecoration(
              icon: Icon(Icons.keyboard),
              border: OutlineInputBorder()
            ),
            controller: num1Controller,
          ),
          SizedBox(height: 30,),
          TextField(
            decoration: InputDecoration(
              icon: Icon(Icons.keyboard),
              border: OutlineInputBorder()
            ),
            controller: num2Controller,
          ),
          SizedBox(height: 30,),
          ElevatedButton(
            onPressed: (){
              result = BigNumber.sum(int.parse(num1Controller.text), int.parse(num2Controller.text));
              setState(() {
                
              });
            }, 
            child: Text("GET SUM")
          ),
          SizedBox(height: 30,),
          Text("RESULT: "+result, style: TextStyle(fontSize: 30, color: Colors.red),),

        ],
      ),
    );
  }
}