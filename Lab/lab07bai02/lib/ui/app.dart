import 'package:flutter/material.dart';
import 'package:lab07bai02/ui/bignumber_util_screen.dart';
import 'package:lab07bai02/ui/string_util_screen.dart';

class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        '/':(context)=> AppHomePage()
      },
      initialRoute: '/',
    );
  }
  
}

class AppHomePage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return AppHomePageState();
  }

}

class AppHomePageState extends State<AppHomePage>{
  int currentIndex = 0;
  final screens = [
    StringUtilScreen(),
    BignumberUtilScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Test Maidatpackages'),
        ),
        body: IndexedStack(
          children: screens,
          index: currentIndex,
        ),
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.greenAccent,
          selectedItemColor: Colors.red,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.wordpress),
              label: 'String Util'
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.numbers),
              label: 'Bignumber Util'
            )
          ],
          currentIndex: currentIndex,
          onTap: (index){
            setState(() {
              currentIndex = index;
            });
          }
        ),
      )
    );
  }
}