import 'package:flutter/material.dart';
import 'package:stringutil/stringutil.dart';

class StringUtilScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return StringUtilScreenState();
  }

}

class StringUtilScreenState extends State<StringUtilScreen>{
  var count = '';
  var normalString = '';
  final stringController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Text("ENTER A STRING", style: TextStyle(fontSize: 30, color: Colors.black)),
          TextField(
            decoration: InputDecoration(
              icon: Icon(Icons.keyboard),
              border: OutlineInputBorder()
            ),
            controller: stringController,
          ),
          ElevatedButton(
            onPressed: (){
              count = StringUtil.countCharacters(stringController.text.toString()).toString();
              setState(() {
                
              });
            }, 
            child: Text("Count words")
          ),
          ElevatedButton(
            onPressed: (){
              normalString = StringUtil.removeVietnameseSymbol(stringController.text.toString());
              setState(() {
                
              });
            }, 
            child: Text("Normalize String")
          ),
          SizedBox(height: 30,),
          Text("Your string's words: "+count, style: TextStyle(fontSize: 15, color: Colors.red)),
          Text("Your normal string is: "+normalString, style: TextStyle(fontSize: 15, color: Colors.red))

        ],
      ),
    );
  }
} 