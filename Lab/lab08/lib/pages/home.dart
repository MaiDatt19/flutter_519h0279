import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return HomeScreenState();
  }

}

class HomeScreenState extends State<HomeScreen>{
  FirebaseFirestore db = FirebaseFirestore.instance;

  @override
  void initState() {
    // db.collection("news").get().then(
    //   (res) => print("Successfully completed"),
    //   onError: (e) => print("Error completing: $e"),
    // );
    db.collection("news").get().then((event) {
      for (var doc in event.docs) {
        print("${doc.id} => ${doc.data()}");
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("TIN TUC"),
        flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [Color.fromARGB(255, 46, 221, 163), Colors.blue]
              )
            ),
          ),
      ),
      body: buildHomeScreen()
      // SingleChildScrollView(
      //   child: Column(
      //     children: [
            
      //     ],
      //   ),
      // )
      
    );
    
  }

  Widget buildHomeScreen(){
    return Container(
            height: double.infinity,
            child: 
                FutureBuilder<QuerySnapshot>(
                  future: db.collection('news').get(),
                  builder:(BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
                    if(snapshot.hasError){
                      return Text('Something went wrong');
                    }
                    if(snapshot.connectionState == ConnectionState.done){
                      var jsonNews = snapshot.data!.docs;
                      return ListView.separated(
                        itemBuilder: (context, index){
                          return ListTile(
                            leading: Container(
                              width: 70,
                              height: 80,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(snapshot.data!.docs[index]['img'].toString())
                                )
                              ),
                            ),
                            title: Text(snapshot.data!.docs[index]['title'].toString(), style: TextStyle(fontSize: 20),),
                            subtitle: Text('\n'+snapshot.data!.docs[index]['src'].toString() +'    '+ snapshot.data!.docs[index]['time'].toString()),
                          );
                          
                          
                          //Text(snapshot.data!.docs[index]['title'].toString());
                        }, 
                        separatorBuilder: (BuildContext context, int index) => const Divider(), 
                        itemCount: snapshot.data!.docs.length
                      );
                    }
                    return Text('Loading');
                  } ,
                )
    );
  }
}