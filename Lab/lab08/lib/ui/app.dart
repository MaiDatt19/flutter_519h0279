import 'package:flutter/material.dart';
import '../pages/home.dart';
import '../pages/trend.dart';
import '../pages/utils.dart';
import '../pages/video.dart';


class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        '/':(context) => AppHomePage()
      },
      initialRoute: '/',
    );
  }

}

class AppHomePage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return AppHomePageState();
  }

}

class AppHomePageState extends State<AppHomePage>{
  var currentIndex = 0;
  final screens = [
    HomeScreen(),
    VideoScreen(),
    TrendScreen(),
    UtilsScreen()
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: AppBar(
        //   title: Text('New Panther'),
        // ),
        body: IndexedStack(
          children: screens,
          index: currentIndex,
        ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          backgroundColor: Colors.white,
          selectedItemColor: Colors.green,
          currentIndex: currentIndex,
          iconSize: 30,
          onTap: (index){
            setState(() {
              currentIndex = index;
            });
          },
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.newspaper),
              label: 'Tin tức'
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.play_arrow),
              label: 'Video'
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.trending_up),
              label: 'Xu hướng'
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.category),
              label: 'Tiện ích'
            ),
          ],
          
        ),
      );
  }
}