import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../model/pizza.dart';


class HttpHelper {
  final String server = '4glvr.mocklab.io';
  final String path = 'pizzalist';
  final String postPath ='pizza';
  final String putPath = 'pizza';
  final String deletePath = 'pizza';

  Future<List<Pizza>> getPizzaList()  async{
    Uri url = Uri.http(server, path);
    http.Response  result = await http.get(url);
    if(result.statusCode == HttpStatus.ok){
      final jsonResponse = json.decode(result.body);
      List<Pizza> pizzas = jsonResponse.map<Pizza>((i)=> Pizza.fromJson(i)).toList();
      return pizzas;
    }
    else{
      return [];
    }
  }

  Future<http.Response> postPizza(Pizza pizza){
    String post = json.encode(pizza.toJson());
    log(post.toString());
    Uri url = Uri.http(server,postPath);
    log(url.toString());
    return http.post(url, 
      headers: {
        'Content-Type': 'application/json;'
      },
      body: post);
  }

  Future<http.Response> putPizza(Pizza pizza){
    String put = json.encode(pizza.toJson());
    Uri url = Uri.http(server,putPath);
    log(url.toString());
    return http.put(url,
        headers: {
          'Content-Type': 'application/json;'
        }, 
        body: put);
  }

  Future<http.Response> deletePizza(int id) async{
    Uri url = Uri.http(server,deletePath);
    log(url.toString());
    return http.delete(url,
      headers: {
        'Content-Type': 'application/json;'
      }, 
      body: id);
  }
}