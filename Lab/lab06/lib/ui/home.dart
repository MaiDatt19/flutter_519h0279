import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:lab06/dataprovider.dart';
import 'package:lab06/ui/add_pizza.dart';
import 'package:http/http.dart' as http;
import 'package:lab06/ui/delete_pizza.dart';

import '../model/pizza.dart';

class HomeScreen extends StatefulWidget{

  const HomeScreen({Key? key}):super(key: key);
  @override
  State<StatefulWidget> createState() {
    return HomeScreenState();
  }

}

class HomeScreenState extends State<HomeScreen>{
  

  @override
  Widget build(BuildContext context) {
    //var pizzaController = DataProvider.of(context);
    return Scaffold(
      body: Column(
        children: [
          Row(
            children: [
              Spacer(),
              TextButton(
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>DeleteScreen()));
                }, 
                child: Text("DELETE MODE"))
            ],
          ),
          FutureBuilder(
            future: readJsonFile(context),
            builder: (BuildContext context, AsyncSnapshot<List<Pizza>> pizzas){
              return ListView.separated(
                shrinkWrap: true,
                itemCount: pizzas.data?.length ?? 0,
                itemBuilder: (BuildContext context, int index){
                  var item = pizzas.data![index];
                  return ListTile(
                    title: Text(item.name),
                    subtitle: Text('${item.description}\nPrice: ${item.price}\$'),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>AddScreen(pizza: item)));
                    },
                  );
                }, separatorBuilder: (BuildContext context, int index) => const Divider(),
              );
            },
          )
        ],
        
      ),
    );
  }

  Future<List<Pizza>> readJsonFile(context) async{
    final res = await http.get(Uri.parse('https://4glvr.mocklab.io/pizzalist'));
    List<Pizza> myPizzas = [];
    if(res.statusCode == 200){
      for (final jsonObj in jsonDecode(res.body)){
        myPizzas.add(Pizza.fromJson(jsonObj));
      }
    }
    else{
      throw Exception('Cant load pizzas');
    }
    return myPizzas;
  }

}