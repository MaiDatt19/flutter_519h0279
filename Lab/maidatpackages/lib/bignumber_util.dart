import 'dart:developer';

class BigNumber {

  static String sum(int a, int b){
    var result = '';
    var text_a = a.toString();
    var text_b = b.toString();
    //can bang so chu so cua a,b
    if(text_a.length > text_b.length){
      text_b = "0"*(text_a.length - text_b.length) + text_b;
    }
    else if(text_a.length < text_b.length){
      text_a = "0"*(text_b.length - text_a.length) + text_a;
    }
    //cong tu chu so cuoi 
    //khoi tao r = 0
    var r = 0;//so du (remainder)

    for(var i=text_a.length-1; i>=0; i--){
      var s1 = int.parse(text_a[i]);
      var s2 = int.parse(text_b[i]);
      if(s1+s2+r > 9){
        result = ((s1+s2+r)%10).toString() + result;
        r = 1;
      }
      else{
        result = (s1+s2+r).toString() + result;
        r = 0;
      }
      
    }
    return result;
  }
}