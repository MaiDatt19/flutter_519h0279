import 'dart:convert';

class ImageModel{
  int? id;
  String? title;
  String? url;
  String? thumbnailUrl;

  ImageModel(this.id, this.title, this.url, this.thumbnailUrl);
  ImageModel.fromJson(jsonObject){
    id = jsonObject['id'];
    title = jsonObject['title'];
    url = jsonObject['url'];
    thumbnailUrl = jsonObject['thumbnailUrl'];
  }
  String toString() {
    return '($id, $title, $url, $thumbnailUrl)';
  }
}