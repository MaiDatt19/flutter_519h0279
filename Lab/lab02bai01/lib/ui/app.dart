import 'package:flutter/material.dart';
import 'package:lab02bai01/validation/mixin_common_validation.dart';
import 'package:flutter/services.dart';

class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Login Me",
      home: Scaffold(
        body: LoginScreen(),
      ),
    );
  }

}

class LoginScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }

}

class LoginState extends State<StatefulWidget> with CommonValidation{
  final formKey = GlobalKey<FormState>();
  late String email;
  late String lname;
  late String fname;
  late String byear;
  late String address;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            emailField(),
            lastNameField(),
            firstNameField(),
            yearField(),
            addressField(),
            loginButton(),
          ],
        )
      )
    );
  }

  Widget emailField(){
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        icon: Icon(Icons.email),
        labelText: "Email address"),
      validator: validateEmail,
      onSaved: (value){
        email = value as String;
      },
    );
  }

  Widget lastNameField(){
    return TextFormField(
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp('[a-zA-Z]')),
      ],
      decoration: InputDecoration(
        icon: Icon(Icons.person),
        labelText: "Last Name"),
      validator: validateLastName,
      onSaved: (value){
        print('onSaved: value=$value');
      },
    );
  }

  Widget firstNameField(){
    return TextFormField(
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp('[a-zA-Z]')),
      ],
      decoration: InputDecoration(
        icon: Icon(Icons.person),
        labelText: "First Name"),
      validator: validateFirstName,
      onSaved: (value){
        print('onSaved: value=$value');
      },
    );
  }

  Widget yearField(){
    return TextFormField(
      keyboardType: TextInputType.number,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.digitsOnly,
      ],
      decoration: InputDecoration(
        icon: Icon(Icons.calendar_view_month),
        labelText: "Birth Year"),
      validator: validateLastName,
      onSaved: (value){
        print('onSaved: value=$value');
      },
    );
  }

  Widget addressField(){
    return TextFormField(
      decoration: InputDecoration(
        icon: Icon(Icons.location_city),
        labelText: "Address"),
      validator: validateAddress,
      onSaved: (value){
        print('onSaved: value=$value');
      },
    );
  }

  Widget loginButton(){
    return ElevatedButton(
      onPressed: (){
        if(formKey.currentState!.validate()){
          formKey.currentState!.save();
        }
      }, 
      child: Text('SUBMIT')
      );
  }


}