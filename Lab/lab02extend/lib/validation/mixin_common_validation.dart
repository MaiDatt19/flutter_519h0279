mixin CommonValidation{
  String? validateEmail(String? value){
    if(value!.isEmpty){
      return "Email is required.";
    }
    else if(!value.contains('@')){
      return "Invalid email (Missing '@')";
    }
    else if (!value.contains('.')){
      return "Invalid email (Missing '.')";
    }
    else{
      return null;
    }
  }

  String? validateLastName(String? value){
    if(value!.isEmpty){
      return "Last Name is required";
    }
    // else if(value.contains(RegExp(r'[0-9]'))){
    //   return "First Name can not contain number";
    // }
    else{
      return null;
    }
  }

  String? validateFirstName(String? value){
    if(value!.isEmpty){
      return "First Name is required";
    }
    // else if(value.contains(RegExp(r'[0-9]'))){
    //   return "First Name can not contain number";
    // }
    else{
      return null;
    }
  }

  String? validateYear(String? value){
    if(value!.isEmpty){
      return "Birth Year is required";
    }
    else{
      return null;
    }
  }

  String? validateAddress(String? value){
    if(value!.isEmpty){
      return "Address is required";
    }
    else{
      return null;
    }
  }

}