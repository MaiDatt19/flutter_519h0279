import 'package:flutter/material.dart';

class TextStream{
  Stream<String> getMessages() async* {
    final List<String> messages = [
      'Welcome everyone!',
      'Thank you for watching',
      'Please donate for me',
      'Support me with donation',
      'What type of videos do you like?',
      'Please wait for a few minutes',
      'Good bye everyone'
    ];

    yield* Stream.periodic(Duration(seconds: 5), (int t){
      int index = t%7;
      return messages[index];
    });

  }
}