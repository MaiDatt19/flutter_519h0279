import 'package:flutter/material.dart';
import 'package:lab04/detail/clothes/man_style.dart';
import 'package:lab04/detail/clothes/woman_style.dart';


class ClothesPage extends StatelessWidget{
  final List<String> clothesList = <String>[
    'Thời trang nam',
    'Thời trang nữ',
    'Thời trang trẻ em',
    'Áo hoodie',
    'Jeans'
  ];
  final List<List<String>> imgList = <List<String>>[
    ['https://sakurafashion.vn/upload/images_665/F115641C-E330-486B-9144-51A46FF87B66.jpeg',
      'https://5.imimg.com/data5/GH/TM/OE/SELLER-18842120/img-20190823-wa0018-500x500.jpg',
      'https://image.dhgate.com/0x0s/f2-albu-g10-M01-F5-61-rBVaWVyks8SAdU7lAADdjruigfw389.jpg/men-shirt-long-sleeve-mens-shirts-casual.jpg'
    ],
    [
      'https://sslimages.shoppersstop.com/sys-master/images/h2c/h8a/16208693887006/S21507CHECKSHT6_RED.jpg_230Wx334H',
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ7eHn29RXeq61zu_-8HdbNro_P10wW3AH_wg&usqp=CAU',
      'https://www.rei.com/media/39f095e7-a79b-46a2-8bb7-0ccffb5d6c9e.jpg'
    ],
    [
      'https://www.sheknows.com/wp-content/uploads/2019/11/Screen-Shot-2020-04-08-at-4.31.33-PM.png?w=679',
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2_8BKSPWF-WNNdViIhT4bxfZ0X9-YMPBAKg&usqp=CAU',
      'https://cdn.shopify.com/s/files/1/0266/6276/4597/products/100001_300880392_039_1.jpg?v=1643373960'
    ],
    [
      'https://m.media-amazon.com/images/I/61z8EVo52YL._AC_SL1500_.jpg',
      'https://product.hstatic.net/1000357687/product/mat_truoc_51d7646169874f4b969ffafa8446a3a4_master.jpg',
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSSFyDeMfbTrvfHfnj4mn9bANoTeo8qAUR04w&usqp=CAU'
    ],
    [
      'https://image.uniqlo.com/UQ/ST3/AsianCommon/imagesgoods/438370/item/goods_63_438370.jpg?width=1600&impolicy=quality_75',
      'https://cf.shopee.vn/file/32979964f2168c41db4d1349c78e106a',
      'https://image.uniqlo.com/UQ/ST3/WesternCommon/imagesgoods/430257/item/goods_09_430257.jpg?width=1600&impolicy=quality_75'
    ]
  ];
  final clothes_screens = [
    MenClothesDetail(),
    WomenClothesDetail()
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            height: 50,
            child: Center(
              child: Text('QUẦN ÁO', 
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20 
                ),
              )
            )
          ),
          const Divider(color: Colors.black, height: 5,),
          Container(
            height: 642,
            padding: EdgeInsets.all(10),
            color: Color.fromARGB(255, 228, 227, 227),
            child: ListView.separated(
            itemBuilder: (BuildContext context, int index){
              return Container(
                height: 120,
                color: Colors.white,
                padding: EdgeInsets.all(5),
                child: Column(
                  children: [
                    //thanh title va nut tat ca
                    Row(
                      
                      children: [
                        Text('${clothesList[index]}', style: TextStyle(fontWeight: FontWeight.bold),),
                        Spacer(),
                        TextButton(
                          onPressed: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => clothes_screens[index]));
                          }, 
                          child: Text('TẤT CẢ')
                        )
                      ],
                    ),
                    //hinh anh san pham
                    Container(
                      height: 60,
                      
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: 3,
                        itemBuilder: (BuildContext context, int imgindex){
                          return Container(
                              width: 60.0,
                              height: 40.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('${imgList[index][imgindex]}')
                                )
                            )
                          );
                        }
                      ),
                    )


                  ],
                ),
              );
            }, 
            separatorBuilder: (BuildContext context, int index) => const Divider(),
            itemCount: 5
          ),
          )
        ]
      ),
    );
  }

}