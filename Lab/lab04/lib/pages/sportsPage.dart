import 'package:flutter/material.dart';
import 'package:lab04/detail/sports/basketball.dart';
import 'package:lab04/detail/sports/football.dart';


class SportsPage extends StatelessWidget{
  final List<String> sportsList = <String>[
    'Bóng đá',
    'Bóng rổ',
    'Cầu lông',
    'Bơi lội',
    'Yoga'
  ];
  final List<List<String>> imgList = <List<String>>[
    [
      'https://images.elipsport.vn/news/2020/12/9/tim-hieu-qua-bong-nang-bao-nhieu-kg.1607509808.jpg',
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQkk3L_3IoNOmh9CjSyig-9Df4McefLrgfkAA&usqp=CAU',
      'https://kiwisport.vn/wp-content/uploads/2020/01/qua-bong-futsal-3030-pro-chuyen-da-san-5.jpg'
    ],
    [
      'https://thethaodonga.com/wp-content/uploads/2021/05/qua-bong-ro.jpg',
      'https://www.thethaothientruong.vn/uploads/contents/qua-bong-ro-molten.jpg',
      'https://meta.vn/Data/image/2019/05/11/bong-ro-spalding-tf-150-performance-outdoor-size-5-fiba-83-599z-1.jpg'
    ],
    [
      'https://luongsport.com/wp-content/uploads/2017/07/KKKKKKKKK-1.jpg',
      'https://vn-test-11.slatic.net/original/16fb214bedeaac5dc8811a03dce83dc9.jpg',
      'https://contents.mediadecathlon.com/p1605540/4997bf2d4aa01126a2fd6c4d20554f1d/p1605540.jpg'
    ],
    [
      'https://trekhoedep.net/wp-content/uploads/2021/06/K%C3%ADnh-b%C6%A1i-ti%E1%BA%BFng-Anh-l%C3%A0-g%C3%AC.jpg',
      'https://img.zanado.com/media/catalog/product/cache/all/thumbnail/360x420/7b8fef0172c2eb72dd8fd366c999954c/5/_/quan_boi_nam_speed_thoi_trang_2206.jpg',
      'https://media3.scdn.vn/img4/2021/04_18/tPzkyqLYtF5uEouKwDkN_simg_b5529c_320x320_maxb.jpg'
    ],
    [
      'https://vn-test-11.slatic.net/p/78480e27447d7fac260152ae08272668.jpg',
      'https://muongicungco.com/wp-content/uploads/2020/09/tham-tap-yoga-tot.jpg',
      'https://thethaominhphu.com/wp-content/uploads/2018/05/tham-tap-yoga-Kitten-Mat.jpg'
    ]
  ];
  final sports_screens = [
    FootballSportsDetail(),
    BasketballSportsDetail(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            height: 50,
            child: Center(
              child: Text('THỂ THAO', 
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20 
                ),
              )
            )
          ),
          const Divider(color: Colors.black, height: 5,),
          Container(
            height: 642,
            padding: EdgeInsets.all(10),
            color: Color.fromARGB(255, 228, 227, 227),
            child: ListView.separated(
            itemBuilder: (BuildContext context, int index){
              return Container(
                height: 120,
                color: Colors.white,
                padding: EdgeInsets.all(5),
                child: Column(
                  children: [
                    //thanh title va nut tat ca
                    Row(
                      
                      children: [
                        Text('${sportsList[index]}', style: TextStyle(fontWeight: FontWeight.bold),),
                        Spacer(),
                        TextButton(
                          onPressed: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => sports_screens[index]));
                          }, 
                          child: Text('TẤT CẢ')
                        )
                      ],
                    ),
                    //hinh anh san pham
                    Container(
                      height: 60,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: 3,
                        itemBuilder: (BuildContext context, int imgindex){
                          return Container(
                              width: 60.0,
                              height: 40.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('${imgList[index][imgindex]}')
                                )
                            )
                          );
                        }
                      ),
                    )


                  ],
                ),
              );
            }, 
            separatorBuilder: (BuildContext context, int index) => const Divider(),
            itemCount: 5
          ),
          )
        ]
      ),
    );
  }

}