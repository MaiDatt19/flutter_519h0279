import 'package:flutter/material.dart';
import 'package:lab04/page_detail/clothes.dart';
import 'package:lab04/pages/clothesPage.dart';
import 'package:lab04/ui/category.dart';


class HintPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container( //-> dong Thu muc dang hot
            height: 50,
            child: Center(
              child: Text('Danh mục đang hot', 
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20 
                ),
              )
            )
          ),
          const Divider(color: Colors.black, height: 5,), //-> duong phan cach
          Container( //=> grid view
            height: 600,
            color: Colors.red,
            child: CustomScrollView(
              primary: false,
              slivers: <Widget>[
                SliverPadding(
                  padding: EdgeInsets.all(10),
                  sliver: SliverGrid.count(
                    crossAxisCount: 2,
                    crossAxisSpacing:  10,
                    mainAxisSpacing: 10,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 218, 236, 241),
                        child: Column(children: [
                          IconButton(
                          icon: Icon(Icons.dry_cleaning), 
                          iconSize: 50,
                          onPressed: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => ClothesDetail()),);
                          },
                        ),
                        Text('Quần Áo', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 218, 236, 241),
                        child: Column(children: [
                          IconButton(
                          icon: Icon(Icons.sports_soccer), 
                          iconSize: 50,
                          onPressed: (){},
                        ),
                        Text('Thể thao', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 218, 236, 241),
                        child: Column(children: [
                          IconButton(
                          icon: Icon(Icons.smartphone), 
                          iconSize: 50,
                          onPressed: (){},
                        ),
                        Text('Điện thoại', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 218, 236, 241),
                        child: Column(children: [
                          IconButton(
                          icon: Icon(Icons.food_bank), 
                          iconSize: 50,
                          onPressed: (){},
                        ),
                        Text('Thực phẩm', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 218, 236, 241),
                        child: Column(children: [
                          IconButton(
                          icon: Icon(Icons.my_library_books), 
                          iconSize: 50,
                          onPressed: (){},
                        ),
                        Text('Sách truyện', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 218, 236, 241),
                        child: Column(children: [
                          IconButton(
                          icon: Icon(Icons.laptop), 
                          iconSize: 50,
                          onPressed: (){
                            
                          },
                        ),
                        Text('Máy tính', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
          
        ]
      ),
    );
  }

}