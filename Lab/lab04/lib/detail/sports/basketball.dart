import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BasketballSportsDetail extends StatefulWidget{
  static const route = '/sports/basketball';
  @override
  State<StatefulWidget> createState() {
    return BasketballSportsDetailState();
  }

}

class BasketballSportsDetailState extends State<BasketballSportsDetail>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget> [
          Container(
            height: 70,
            color: Colors.blue,
            child: Row(
              children: [
                IconButton(
                  icon: Icon(Icons.arrow_back),
                  color: Colors.white,
                  onPressed: (){
                    Navigator.pop(context);
                  },
                ),
                Container(
                  width: 300,
                  child: CupertinoSearchTextField(
                    backgroundColor: Colors.white,
                    placeholder: 'Bóng rổ',
                  ),
                )
              ],
            ),
          ),
          Container(
            height: 795,
            child: CustomScrollView(
              primary: false,
              slivers: <Widget>[
                SliverPadding(
                  padding: EdgeInsets.all(10),
                  sliver: SliverGrid.count(
                    crossAxisCount: 2,
                    crossAxisSpacing:  10,
                    mainAxisSpacing: 10,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://thetigertribune.com/wp-content/uploads/2020/10/basketball.jpg')
                                )
                            )
                          ),
                          Text('300.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Bóng rổ', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://www.spalding.com/dw/image/v2/ABAH_PRD/on/demandware.static/-/Sites-masterCatalog_SPALDING/default/dwd1aedc60/images/hi-res/77015E__FRONT.jpg?sw=338&sh=426&sm=cut&sfrm=jpg')
                                )
                            )
                          ),
                          Text('300.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Bóng rổ', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTpez6j5x8ZjUnBOG80V49w8rUVyqJOcboFkQtQuqToQ8FKIdWZDTwEn1wxyPQwfUlBoFU&usqp=CAU')
                                )
                            )
                          ),
                          Text('300.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Bóng rổ', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRN1icmRRSy7t89Hgb-39ezC21uA5ucLZc32cGCmRcRJB4QFeZIyIyVoUFjZRjE6OPzCSc&usqp=CAU')
                                )
                            )
                          ),
                          Text('300.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Bóng rổ', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfG0fLye2LUSwpE73mHYXxxRiTjproxakT75SIAYmlqc1m_yylxFNtxhCWkMVkYZhQnuE&usqp=CAU')
                                )
                            )
                          ),
                          Text('300.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Bóng rổ', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSRLsVgu8zaivb8APpBMSP8SYVbiyQHvQLsQPRhyU-173BpF3BMEU0qtakV6mEpwONgp60&usqp=CAU')
                                )
                            )
                          ),
                          Text('300.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Bóng rổ', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSOIZdK-qPy0DwZ1beO0XKyA40D9RYVvbalm9ZjO1MqcVChjPTgasmlncj79FkWzIKfpME&usqp=CAU')
                                )
                            )
                          ),
                          Text('300.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Bóng rổ', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT6V19tuygCCcZwgGGUJJSIAJJvycOj1xrxTtx9kynSnZZ73Nj1JEhFyOGegti3fVjAmB8&usqp=CAU')
                                )
                            )
                          ),
                          Text('300.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Bóng rổ', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

}