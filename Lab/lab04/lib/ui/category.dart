import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../pages/clothesPage.dart';
import '../pages/hintPage.dart';
import '../pages/sportsPage.dart';

class CategoryScreen extends StatefulWidget{
  static const route = '/category';

  @override
  State<StatefulWidget> createState() {
    return CategoryScreenState();
  }
  
}

class CategoryScreenState extends State<CategoryScreen> {
  int index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: 70,
            color: Colors.blue,
            padding: EdgeInsets.all(10),
            child: CupertinoSearchTextField(
              backgroundColor: Colors.white,
              placeholder: 'Sản phẩm, thương hiệu và mọi thứ bạn cần...',
            ),
          ),
          Container(
            height: 697,
            child:  Row(
              children: <Widget>[
              //thanh menu ben trai
              NavigationRail(
                backgroundColor: Color.fromARGB(255, 219, 235, 255),
                selectedIndex: index,
                selectedIconTheme: IconThemeData(color: Colors.blueAccent, size: 50),
                unselectedIconTheme: IconThemeData(color: Colors.grey, size: 30),
                onDestinationSelected: (int index){
                  setState(() {
                    this.index = index;
                  });
                },
                labelType: NavigationRailLabelType.all,
                destinations: [
                  NavigationRailDestination(
                    icon: Icon(Icons.star_sharp, color: Color.fromARGB(255, 255, 209, 4),), 
                    label: Text('Gợi ý cho bạn'),
                  ),
                  NavigationRailDestination(
                    icon: Icon(Icons.dry_cleaning,), 
                    label: Text('Quần áo')
                  ),
                  NavigationRailDestination(
                    icon: Icon(Icons.sports_soccer, ),
                    label: Text('Thể thao')
                  ),
                  NavigationRailDestination(
                    icon: Icon(Icons.smartphone,), 
                    label: Text('Điện thoại')
                  ),
                  NavigationRailDestination(
                    icon: Icon(Icons.food_bank,), 
                    label: Text('Thực phẩm')
                  ),
                  NavigationRailDestination(
                    icon: Icon(Icons.my_library_books,), 
                    label: Text('Sách truyện')
                  ),
                  NavigationRailDestination(
                    icon: Icon(Icons.laptop,), 
                    label: Text('Máy tính')
                  ),
                ],
              ),
              VerticalDivider(thickness: 2, width: 2, color: Colors.black,),
              Expanded(child: buildPages()),
              

              //list san pham

              
            ],
          ),
          )
         
        ],
      ),
    );
    
  }

  Widget buildPages(){
      switch (index){
        case 0:
          return HintPage();
        case 1:
          return ClothesPage();
        case 2:  
          return SportsPage();
        default:  
          return defaultPage();
      }
    }

  
}

class defaultPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: Text('This page is setting as Default Page.')),
    );
  }

}